/*
Ejemplo. SOA.
 */
package pacoaldariasdwst8e40;

/**
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 */
public class Pacoaldariasdwst8e40 {

   /**
    * @param args the command line arguments
    */
   public static void main(String[] args) {
      String cad = "";

      cad = getCitiesByCountry("Spain");

      System.out.println(cad);
   }

   private static String getCitiesByCountry(java.lang.String countryName) {
      net.webservicex.GlobalWeather service = new net.webservicex.GlobalWeather();
      net.webservicex.GlobalWeatherSoap port = service.getGlobalWeatherSoap();
      return port.getCitiesByCountry(countryName);
   }

}
